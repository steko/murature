#! /usr/bin/env python2.5
# -*- coding: utf-8 -*-
# Copyright (C) 2008 Stefano Costa
# Filename: genericplot.py
# This file is part of the IOSA project

# IOSA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# IOSA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with IOSA.  If not, see <http://www.gnu.org/licenses/>.

from rpy import *
import matplotlib
from pylab import *


class GenericPlot:
    '''Useful plots and summaries to compare different measurement methods.'''
    
    def __init__(self, *data, **plotpar):
        """Plots all the datasets passed as arguments.
        
        Pass the datasets to be plotted as a single tuple containing
        dictionaries. Each dict must contain at least the "data" and "name"
        keys, and the optional "magic" key.
        """
        
        self.datasets = data
        
        self.main_title = plotpar['main']
        self.x_label = plotpar['x_label']
        self.y_label = plotpar['y_label']
    
    
    def r_plot_commands(self):
        '''Plot commands common to all drawing libraries.'''
        
        color = 1
        r.par(cex=0.8)
        r.par(col=color)
        r.plot(r.as_list(r.environment(r.ecdf(self.datasets[0]['data']))),
            do_p=False,
            verticals=True,
            main=self.main_title,
            xlab=self.x_label,
            ylab=self.y_label,
            type='n',
            xlim = [0, 10])
        for i in self.datasets:
            r.par(col=color)
            r.lines(r.as_list(r.environment(r.ecdf(i['data']))),type="s")
            color = color +1
        r.par(col="black")
        r.legend("topleft",
            [ elem['name'] for elem in self.datasets],
            bg="grey90",
            fill = [ num +1 for num in range(len(self.datasets))])
    
    def r_cairo_plot(self, commands, outfile="Jizz_plot.png"):
        '''It can be used to save a PNG file in the working directory.
        
        This version outputs good-looking Cairo anti-aliased bitmaps.'''
        
        comy = getattr(self, commands, self.r_plot_commands)
        try:
            r.library('cairoDevice')
        except RException:
            print("""The cairoDevice R library is missing.
            
            Falling back to the default graphics library.""")
            r.png(outfile, width=1000, height=600)
        else:
            r.Cairo(width=10, height=6, filename=outfile)
        finally:
            comy()
            r.dev_off()
    
    def mat_plot(self, outfile="generic_plot.png"):
        '''Plot using matplotlib.'''
        
        xmax = 0
        for i in self.datasets:
            dat = i['data']
            if max(dat) > xmax: xmax = max(dat)
        
        for i in self.datasets:
            '''Empirical Cumulative Distribution Function.'''
            
            dat = i['data']
            n = len(dat)
            sted = dat
            sted.sort()
            pdat = [ k / float(n) for k in range(n)] # ECDF
            hold(True)
            plot(sted, pdat)
        
        # the title and labels do not appear, but the grid does...
        title = self.main_title
        xlabel = self.x_label
        ylabel = self.y_label
        grid()
        leglist = []
        for item in self.datasets:
            if 'magic' in item:
                val = item['name']+", n="+str(item['magic'])
            else:
                val = item['name']
            print val
            leglist.append(val)
        
#        def item_legend(item): ('magic' in item) and (lambda item: item['name']+", n="+str(item['magic'])) or (lambda item: item['name'])
#        print [ item_legend(item) for item in self.datasets ]
        
        legend(tuple(leglist), loc=0)
        xlim(xmax=xmax+1)
        #savefig(outfile)
        show()


if __name__ == "__main__":
    from geometrywall import *
    
    magic_number = 5.0
    magic_number_2 = 15.0
    
    sanNicolo = GeometryWall('data/gianluca.shp')
    sanNicolo.doWhatIExpect(magic_number, magic_number_2)
    
    graph = GenericPlot({'data':sanNicolo.simple_h_list,'name':'simple'},
                        {'data':sanNicolo.smart_h_list,'name':'smart','magic':magic_number},
                        {'data':sanNicolo.smart_2_h_list,'name':'smart2','magic':magic_number_2},
                        {'data':sanNicolo.hand_list,'name':'manual'},
                        main="Test",
                        x_label="Stone height",
                        y_label="Cumulative %")
    
    graph.mat_plot()

