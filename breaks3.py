#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2008 Stefano Costa <steko@iosa.it>
# Filename: breaks3.py
# This file is part of the IOSA project.

# IOSA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# IOSA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with IOSA.  If not, see <http://www.gnu.org/licenses/>.

from numpy import *
from scipy.stats import gaussian_kde


class steko_kde(gaussian_kde):
    def steko_factor(self):
        return power(self.n, -3./(self.d+4))
    
    covariance_factor = steko_factor

def do_breaks(data):
    '''Finds breaks in the density distribution of stones' coordinates, to
    determine in which row the stones are.
    '''
    d = array(data.values())
    k = steko_kde(d)

    x = linspace(d.min(), d.max(), 100)
    y = k(x)
    
    breaks = [0]
    y2 = list(y)
    x2 = list(x)
    
    for n in y2:
        y_index = y2.index(n)
        y_length = len(y) - 1
        if y_index > 0 and y_index < y_length:
            prev = y2[y_index-1]
            next = y2[y_index+1]
            if prev > n and next > n:
                breaks.append(x2[y_index])
    breaks.append(max(data.values()) + 1)
    result = {}
    
    for ob in data:
        for br in breaks:
            ind = breaks.index(br)
            prev = breaks[ind-1]
            if prev < data[ob] < br:
                result[ob] = breaks.index(br)
    return result

