==================================
 from DXF to geometry using GRASS
==================================

In many cases, wall vector drawings come in DXF format, especially if they
were created using Autocad.

This means that a little extra work is needed before we can start processing
the file, mainly because all stones are stored as ``POLYLINE`` or the
even worse ``LINE`` object type.

At the moment there is no automatic procedure for handling this conversion, so
our solution was to use the `GRASS GIS <http://grass.osgeo.org/>`_ to obtain
the clean vector. We recommend to use the development version (6.3).

Start GRASS creating a new, empty *XY* location, and call it as you like.

We will proceed, as usual, creating all the needed intermediate vectors. To
better keep track of the process, we add numbers after the common filename.
Remember not to make names start with numbers, because this is illegal in SQL
and therefore also in GRASS.

All commands here are given in their command line form, but you can always use
the GUI panels. Just be sure to select the right options.

Import the DXF file with ``v.in.dxf``. You might want to import just one layer,
so here's how to get a list of all the layers in the DXF::

  v.in.dxf -l input=/home/steko/iscum/CampanileInterno.dxf

Chances are that, whatever you have in your DXF, the boundaries of stones will
be in a single layer (we assume this is your situation in the following steps).
However, your mileage may vary, again it's better to check your file *before*
you start.

Once you have decided which layer you want to import, just remove the ``-l``
flag and pass the command the right layer's name and output name::

  v.in.dxf input=/home/steko/iscum/CampanileInterno.dxf output=campanile_1 layers=stones

Be paranoid: check that the new vector has been correctly created::

  v.info campanile_1
  
  +----------------------------------------------------------------------------+
  | Layer:           campanile_1                                               |
  | Mapset:          PERMANENT                                                 |
  | Location:        snicolo                                                   |
  | Database:        /home/steko/grassdata                                     |
  | Title:           campanile_1                                               |
  | Map scale:       1:2400                                                    |
  | Map format:      native                                                    |
  | Name of creator: steko                                                     |
  | Organization:    GRASS Development Team                                    |
  | Source date:                                                               |
  |----------------------------------------------------------------------------|
  |   Type of Map:  vector (level: 2)                                          |
  |                                                                            |
  |   Number of points:       0               Number of areas:      0          |
  |   Number of lines:        4449            Number of islands:    0          |
  |   Number of boundaries:   0               Number of faces:      0          |
  |   Number of centroids:    0               Number of kernels:    0          |
  |                                                                            |
  |   Map is 3D:              1                                                |
  |   Number of dblinks:      1                                                |
  |                                                                            |
  |         Projection: x,y                                                    |
  |               N:      622.06715497    S:       39.03486882                 |
  |               E:        682.549006    W:      364.49612659                 |
  |               B:                 0    T:                 0                 |
  |                                                                            |
  |   Digitization threshold: 0                                                |
  |   Comments:                                                                |
  |                                                                            |
  +----------------------------------------------------------------------------+

This vector contains just lines and has no topology, so retrieving stones at
this point is still impossible. The attribute table is almost useless, because
it just contains a unique ID (``cat`` in GRASS jargon) for each *line*.

You can build polylines from lines, ignoring their attributes::

  v.build.polylines input=campanile_1 output=campanile_2_polylines cats=no 

This new vector is missing an attribute table, as can be seen in the ``v.info``
output:: 

  v.info campanile_2_polylines | grep dblinks
  |   Number of dblinks:      0                                                |

For creating areas, we need *boundaries*, not polylines::

  v.type.sh input=campanile_2_polylines output=campanile_3_boundaries {type=line to boundary}

Add centroids to boundaries and you finally get areas::

  v.centroids input=campanile_3_boundaries output=campanile_4_boundaries_and_centroids option=add layer=1 cat=1 step=1

I'm not sure why there's a large number of islands, but anyway we have to get 
rid of them. Extract only valid data from our vector::

  v.extract input="campanile_4_boundaries_and_centroids" output="campanile_5_areas_clean" type="centroid,area" layer=1 new=-1

Add a table linked to the *clean* vector. This table only contains a primary
key for each geometry::

  v.db.addtable map=campanile_5_areas_clean

