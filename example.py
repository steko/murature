#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2008 Stefano Costa
# Filename: example.py
# This file is part of the IOSA project.

# IOSA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# IOSA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with IOSA.  If not, see <http://www.gnu.org/licenses/>.

import sys

from geometrywall import *
from rplots import *

if len(sys.argv) > 1:
    magicNumber = float(sys.argv[1])
else:
    magicNumber = 5.0

magic_number_2 = 15.0

sanNicolo = GeometryWall('data/gianluca.shp')
sanNicolo.doWhatIExpect(magicNumber, magic_number_2)


grafico = RPlots('misure-pietre-snicolo.csv',
                'Altezza',
                magicNumber,
                magic_number_2,
                sanNicolo.simple_h_list,
                sanNicolo.smart_h_list,
                sanNicolo.smart_2_h_list,
                sanNicolo.hand_list)
grafico.printAndCompare()
#grafico.plotAndCompare()
grafico.cairo_plot()

#sanNicolo.saveToFile()
