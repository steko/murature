#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2008 Stefano Costa
# Filename: rplots.py
# This file is part of the IOSA project.

# IOSA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# IOSA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with IOSA.  If not, see <http://www.gnu.org/licenses/>.

import sys

try:
    from rpy import *
except:
    sys.exit('The RPy library is missing.')

class RPlots:
    '''Useful plots and summaries to compare different measurement methods.'''
    
    def __init__(self, fileName, columnName, magicNumber, magic_number_2, simpleList, smartList, smart_2_heights, hand_list):
#        heightsDataFrame = r.read_delim(fileName,
#                                        header=True,
#                                        na_strings="-",
#                                        sep=',',
#                                        dec=".",
#                                        quote='"')
#        heightsColumn = heightsDataFrame[columnName]
        heightsColumn = hand_list
        self.mainTitle = "Analysis performed on %d stones" % len(simpleList)
        self.x_lab = "Stone height (cm)"
        self.y_lab = "Quantiles"
        self.smart_legend = "smart, n = %5.2f" % (magicNumber)
        self.smart_legend_2 = "smart 2, n = %5.2f" % (magic_number_2)
        self.heightsByHand = r.na_omit(heightsColumn)
        self.simpleList = simpleList
        self.smartList = smartList
        self.smart_2_heights = smart_2_heights
    
    def plot_commands(self):
        '''Plot commands common to all drawing libraries.'''
        
        r.par(cex=0.8)
        r.par(col=1)
        r.plot(r.ecdf(self.heightsByHand),
            do_p=False,
            verticals=True,
            main=self.mainTitle,
            xlab=self.x_lab,
            ylab=self.y_lab)
        r.par(col=2)
        r.lines(r.as_list(r.environment(r.ecdf(self.simpleList))))
        r.par(col=3)
        r.lines(r.as_list(r.environment(r.ecdf(self.smartList))))
        r.par(col=4)
        r.lines(r.as_list(r.environment(r.ecdf(self.smart_2_heights))))
        r.par(col="black")
        r.legend("topleft",
            [ "by hand",
            "simple",
            self.smart_legend,
            self.smart_legend_2 ],
            bg="grey90",
            fill = [ 1,
                     2,
                     3,
                     4 ])
        r("""grid()""")
    
    def plotAndCompare(self):
        '''It can be used to save a PNG file in the working directory.'''
        
        r.png("plotR.png", width=1000, height=600)
        self.plot_commands()
        r.dev_off()
        
    def cairo_plot(self):
        '''It can be used to save a PNG file in the working directory.
        
        This version outputs beautiful anti-aliased bitmaps.'''
        
        r.library('cairoDevice')
        r("""Cairo_png("caiRo_plot.png", height=6, width=10)""")
        self.plot_commands()
        r("""dev.off()""")
    
    def printAndCompare(self):
        '''Print a 4-variable summary.'''
        
        print('by hand')
        r.print_(r.summary(self.heightsByHand))
        print('Simple Height')
        r.print_(r.summary(self.simpleList))
        print('Smart Height')
        r.print_(r.summary(self.smartList))
        print('Smart 2 Height')
        r.print_(r.summary(self.smart_2_heights))

