#! /usr/bin/env python
# -*- coding: utf-8 -*-

import osgeo.ogr as ogr
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

from shapely.wkb import loads

Path = mpath.Path

fig = plt.figure()
ax = fig.add_subplot(111)

source = ogr.Open("data/gianluca.shp")
borders = source.GetLayer(0)
counts = borders.GetFeatureCount()

x_min, x_max, y_min, y_max = borders.GetExtent()
x_ext = (x_max - x_min) / 20
y_ext = (y_max - y_min) / 20

for i in range(counts):
    feature = borders.GetNextFeature()
    stone = loads(feature.GetGeometryRef().ExportToWkb())
    
    # example of color by attribute
    attribute = feature.GetFieldAsDouble('handheight')
    if attribute < 15 and attribute > 10:
        color = 'red'
    else:
        color='white'
    
    path = mpath.Path(stone.boundary.coords)
    patch = mpatches.PathPatch(path, facecolor=color, edgecolor='black')
    ax.add_patch(patch)

ax.grid()
ax.set_xlim(x_min - x_ext, x_max + x_ext)
ax.set_ylim(y_min - y_ext, y_max + y_ext)
ax.axis('equal')
ax.set_title(u'San Nicolò')
plt.show()

