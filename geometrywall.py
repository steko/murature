#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2008 Stefano Costa
# Filename: geometrywall.py
# This file is part of the IOSA project.

# IOSA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# IOSA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with IOSA.  If not, see <http://www.gnu.org/licenses/>.

import random
import sys
import math

try:
    import breaks3 as breaks
except ImportError:
    try:
        import breaks
    except ImportError:
        bineng = False
    else:
        bineng = True
else:
    bineng = True

try:
    from osgeo.ogr import *
except ImportError:
    try:
        from ogr import *
    except ImportError:
        sys.exit('The OGR Python bindings are missing.')


def simpleAlgo(listey):
    '''basic algo for height'''
    yMin = min(listey)
    yMax = max(listey)
    yD = yMax - yMin
    return yD


def smartAlgo(listey,ratio):
    '''`smart' algo with average coordinates.'''
    listey.sort()
    asd = 0
    for i in listey[0:ratio]:
        asd = asd + i
    yMin = asd/ratio

    asd = 0
    for i in listey[-ratio:]:
        asd = asd + i
    yMax = asd/ratio

    yAvg = yMax - yMin
    return yAvg

def smart_2(listey,ratio2):
    '''Another `smart' algo.'''

    simple_h = simpleAlgo(listey)
    y_min = min(listey)
    y_max = max(listey)
    prop = simple_h / ratio2
    
    # upper limit
    limit = y_max - prop
    n_of_points = 0
    total_to_average = 0
    for y in listey:
        if limit < y <= y_max:
            n_of_points = n_of_points + 1
            total_to_average = total_to_average + y
    upper_average_limit = total_to_average / n_of_points
    
    # lower limit
    limit = y_min + prop
    n_of_points = 0
    total_to_average = 0
    for y in listey:
        if y_min <= y < limit:
            n_of_points = n_of_points + 1
            total_to_average = total_to_average + y
    lower_average_limit = total_to_average / n_of_points
    
    smart_2_height = upper_average_limit - lower_average_limit
    return smart_2_height

class GeometryWall:
    '''Load a valid OGR datasource containing polygons, one for each stone.
    
    Then the datasource is copied to the 'Memory' OGR driver, to be able
    to add fields. At the end of all procedures, the resulting geometry
    layer can be exported to another file.'''
    
    def __init__(self, filePath):
        self.dataTable = []
        
        originalFile = Open(filePath)
        memory = GetDriverByName('Memory')
        self.workingFile = memory.CopyDataSource(originalFile,'TemporaryDS')
        originalFile.Release()
        self.workingLayer = self.workingFile[0]
        self.numberOfStones = self.workingLayer.GetFeatureCount()
        field1 = FieldDefn('simpleH',2) # real
        field1.SetPrecision(2)
        field2 = FieldDefn('smartH',2) # real
        field_s2 = FieldDefn('smart2height',2) # real
        field3 = FieldDefn('HDiff',2) # real
        field4 = FieldDefn('corso',0) # integer
        self.workingLayer.CreateField(field1)
        self.workingLayer.CreateField(field2)
        self.workingLayer.CreateField(field_s2)
        self.workingLayer.CreateField(field3)
        self.workingLayer.CreateField(field4)
    
    def loadAndCompute(self, magicNumber, magic_number_2):
        '''Loads features = stones from the working layer.
        
        Also gets a number of geometry data needed after.'''
        
        self.workingLayer.ResetReading()
        
        for i in range(self.numberOfStones):
            '''Semi-pythonic way of going through the layer, getting all Ys.'''
            
            self.currentStone = self.workingLayer.GetNextFeature()
            self.stoneID = self.currentStone.GetFID()
            
            stoneGeometry = self.currentStone.GetGeometryRef()
            self.stoneCentroidY = stoneGeometry.Centroid().GetY()
            
            stoneBoundary = stoneGeometry.GetBoundary()
            #stoneBoundary.GetGeometryName() # == 'LINESTRING'
            
            self.stonePointsCount = stoneBoundary.GetPointCount()
            self.PointsYList = []
            for i in range(self.stonePointsCount):
                self.PointsYList.append(stoneBoundary.GetY(i))
            hand_height = stoneGeometry = self.currentStone.GetFieldAsDouble('handheight')
            lowest_point_y = min(self.PointsYList)
            
            pointsRatio = int(( self.stonePointsCount / magicNumber ) + \
                (math.pow((self.stonePointsCount / magicNumber),0.1) )) # to add or not to add 1 ?
            simpleH = simpleAlgo(self.PointsYList)
            smartH = smartAlgo(self.PointsYList, pointsRatio)
            smart2 = smart_2(self.PointsYList, magic_number_2)
            if hand_height > 0:
                HDiff = abs(smart2 - hand_height)
            else:
                HDiff = None
            
            self.dataTable.append([ self.stoneID,
                                    self.stonePointsCount,
                                    pointsRatio,
                                    simpleH,
                                    smartH,
                                    smart2,
                                    HDiff,
                                    self.stoneCentroidY,
                                    lowest_point_y,
                                    hand_height])
            self.currentStone.SetField('simpleH', simpleH)
            self.currentStone.SetField('smartH', smartH)
            self.currentStone.SetField('smart2height', smart2)
            self.currentStone.SetField('HDiff', HDiff)
            self.workingLayer.SetFeature(self.currentStone) # suggested by Frank Warmerdam!
        
        # store height values in 3 lists for easy processing
        self.simple_h_list = [ record[3] for record in self.dataTable ]
        self.smart_h_list = [ record[4] for record in self.dataTable ]
        self.smart_2_h_list = [ record[5] for record in self.dataTable ]
        self.hand_list = [ record[9] for record in self.dataTable if record[9] > 0.0]
        
    def saveToFile(self):
        '''Save the temporary data source to a file on disk.
        
        Default file format is GML.'''
        saveDriver = GetDriverByName('GML')
        randomNumber = str(random.randint(0,1000000)+1000000)[1:]
        outFile = '%s.gml' %(randomNumber)
        newFileOnDisk = saveDriver.CopyDataSource(self.workingFile, outFile)
        newFileOnDisk.Destroy()
    
    def centroidsY(self):
        '''Returns centroid's Y coordinate for each stone, as a dictionary.
        
        Stone ID becomes the key and the coordinate the value.'''
        
        self.centroids = {}
        for i in self.dataTable:
            self.centroids[i[0]] = i[6]
        return self.centroids
    
    def lowest_y(self):
        '''Returns stone's lowest Y coordinate for each stone, as a dictionary.
        
        Stone ID becomes the key and the coordinate the value.'''
        
        self.lowest = {}
        for i in self.dataTable:
            self.lowest[i[0]] = i[7]
        return self.lowest
    
    def giveRowsData(self):
        '''Returns row attribute for each stone, as a dictionary.
        
        Stone ID becomes the key and the row number the value.'''
        
        rows = breaks.do_breaks(self.lowest_y())
        return rows
    
    def storeRowsData(self, rowsData):
        '''Save rows data as attribute of stones in the OGR datasource.
        
        If we save our datasource, we can reuse this attribute later, for
        calculating some parameter over each row. It's also necessary for
        visualizing each row with a different color.'''
        
        self.workingLayer.SetNextByIndex(0)
        
        for i in range(self.numberOfStones):
            currentStone = self.workingLayer.GetNextFeature()
            stoneID = currentStone.GetFID()
            if stoneID in rowsData:
                row_id = rowsData[stoneID]
                currentStone.SetField('corso', row_id)
            self.workingLayer.SetFeature(currentStone) # suggested by Frank Warmerdam!
    
    def get_rid_of_extra_stones(self, max_number):
        self.workingLayer.SetNextByIndex(0)
        
        for i in range(self.numberOfStones):
            currentStone = self.workingLayer.GetNextFeature()
            row = currentStone.GetFieldAsInteger('corso')
            stoneID = currentStone.GetFID()
            if row > max_number:
                self.workingLayer.DeleteFeature(stoneID)
    
    def create_buffers(self):
        '''Create a polygon buffer for each stone.'''
        #originalFile = ogr.Open('/home/steko/code/murature-luca/pietre-buone.shp')
        #memory = ogr.GetDriverByName('Memory')
        #workingFile = memory.CopyDataSource(originalFile,'TemporaryDS')
        #workingLayer = workingFile[0]
        

        memory = GetDriverByName('Memory')
        buffer_ds = memory.CreateDataSource('temp')
        buffer_layer = buffer_ds.CreateLayer('buffer')
        feature_definition = FeatureDefn()
        field_definition = FieldDefn('stone_id',0) # integer
        feature_definition.AddFieldDefn(field_definition)
        
        total_ds = memory.CreateDataSource('total')
        total_layer = total_ds.CreateLayer('total')
        total_layer.CreateField(field_definition)
        
        feat1 = self.workingLayer.GetFeature(1)
        geom1 = feat1.GetGeometryRef()
        
        self.total_geometry = CreateGeometryFromWkt(geom1.ExportToWkt())
        
        self.workingLayer.ResetReading()
        buffer_layer.ResetReading()
        
        for i in range(self.numberOfStones):
            
            old_feature = self.workingLayer.GetNextFeature()
            old_geometry = old_feature.GetGeometryRef()
            old_feature_height = old_feature.GetFieldAsDouble('smart2height')
            old_fid = old_feature.GetFID()
            
            new_total_geometry = old_geometry.Union(self.total_geometry)
            del(self.total_geometry)
            self.total_geometry = new_total_geometry
            buffer_geometry = old_geometry.Buffer(math.sqrt(old_feature_height))
            new_feature = Feature(feature_definition)
            new_feature.SetGeometry(buffer_geometry)
            buffer_layer.CreateFeature(new_feature)
            new_feature.SetField('stone_id', old_fid)
            buffer_layer.SetFeature(new_feature)
        
        convex_layer = total_ds.CreateLayer('convex')
        total_convex = self.total_geometry.ConvexHull()
        total_feature = Feature(feature_definition)
        total_feature.SetGeometry(self.total_geometry)
        total_layer.CreateFeature(total_feature)
        
        gml = GetDriverByName('GML')
        buffer_file = gml.CopyDataSource(buffer_ds,'buffer.gml')
        total_file = gml.CopyDataSource(total_ds,'total.gml')
        buffer_file.Destroy()
        total_file.Destroy()
        
        buffer_layer.ResetReading()
        field_buffer = FieldDefn('buffer_value',2) # real
        self.workingLayer.CreateField(field_buffer)
        
        for i in range(buffer_layer.GetFeatureCount()):
            buffer = buffer_layer.GetNextFeature()
            stone_id = buffer.GetFieldAsInteger('stone_id')
            buffer_geometry = buffer.GetGeometryRef()
            buffer_area = buffer_geometry.GetArea()
            intersection = buffer_geometry.Intersection(self.total_geometry)
            inters_area = intersection.GetArea()
            #value = math.log((buffer_area - inters_area) / buffer_area)
            stone = self.workingLayer.GetFeature(stone_id)
            stone_geometry = stone.GetGeometryRef()
            stone_area = stone_geometry.GetArea()
            stone_boundary = stone_geometry.GetBoundary()
            #stone_2p = stone_boundary.GetLength()
            mortar_area = buffer_area -inters_area
            #print "%s = %s - %s - %s" % (mortar_area, buffer_area, stone_area, inters_area)
            #value = math.log((buffer_area - stone_area) / (inters_area - stone_area))
            value = math.sqrt(mortar_area) / math.sqrt(stone_area)
            stone.SetField('buffer_value', value)
            self.workingLayer.SetFeature(stone)
        
    def doWhatIExpect(self, magicNumber, magic_number_2):
        '''Executes all methods in the right order.
        
        Are all these methods useless, then?'''
        
        self.loadAndCompute(magicNumber, magic_number_2)
        if bineng:
            self.storeRowsData(self.giveRowsData())
#        self.create_buffers()

