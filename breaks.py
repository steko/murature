#! /usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2008 Stefano Costa <steko@iosa.it>
# Filename: breaks.py
# This file is part of the IOSA project.

# IOSA is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# IOSA is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with IOSA.  If not, see <http://www.gnu.org/licenses/>.

import sys

try:
    from rpy import *
except ImportError:
    sys.exit('The RPy library is missing.')


def do_breaks(data, bandw=3.0):
    '''Finds breaks in the density distribution of stones' coordinates, to
    determine in which row the stones are.
    '''
    
    floatData = [ float(i) for i in data.values() ]
    densityData = r.density(floatData, bw=bandw)
    #return densityData
    
    breaks = [0]
    
    density_y = densityData['y']
    density_x = densityData['x']
    
    for n in density_y:
        density_y_index = density_y.index(n)
        density_y_length = len(density_y) - 1
        if density_y_index > 0 and density_y_index < density_y_length:
            prev = density_y[density_y_index-1]
            next = density_y[density_y_index+1]
            if prev > n and next > n:
                breaks.append(density_x[density_y_index])
    breaks.append(max(data.values()) + 1)
    result = {}
    
    for ob in data:
        for br in breaks:
            ind = breaks.index(br)
            prev = breaks[ind-1]
            if prev < data[ob] < br:
                result[ob] = breaks.index(br)
    return result

